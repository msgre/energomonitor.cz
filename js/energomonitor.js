/**
 * Energomonitor Frontend
 * @author CZECHGEEKS, s.r.o.
 */


/**
 * Class constructor
 * @constructor
 */
function Energomonitor_frontend() {

    //Today
    this._todayCostEl = $("#cost-today");
    this._todayKwhEl = $("#kwh-today");

    //Yesterday
    this._yesterdayCostEl = $("#cost-yesterday");
    this._yesterdayKwhEl = $("#kwh-yesterday");
    this._wholeYesterdayCostEl = $("#yesterday-whole");

    //Current month
    this._currentMonthNameEls = $(".current-month-name");
    this._currentMonthCostEl = $("#cost-this-month");
    this._currentMonthKwhEl = $("#kwh-this-month");

    //Last month
    this._lastMonthNameEls = $(".last-month-name");
    this._lastMonthCostEl = $("#cost-last-month");
    this._lastMonthKwhEl = $("#kwh-last-month");
    this._wholeLastMonthCostEl = $("#last-month-whole");

    this.checkSelectboxes(null);
    $(".select ul.dropdown-menu li a").click(this._clickOnDropdown);
}

/**
 * Today COST setter
 * @param cost (integer)
 * @param up (true || false || null)
 */
Energomonitor_frontend.prototype.setTodayCost = function (cost, up) {
    this._todayCost = cost || 0;
    this._todayCostUp = (typeof up == "boolean") ? up : null;
    this._todayCostEl.text(this._todayCost);
    this._setTodayCostArrow(this._todayCostUp);
}

/**
 * Today COST arrow setter
 * @param up (true || false || null)
 * @private
 */
Energomonitor_frontend.prototype._setTodayCostArrow = function (up) {
    this._todayCostUp = (typeof up == "boolean") ? up : null;
    this._todayCostEl.attr("data-up", this._todayCostUp);

    var arrow = this._todayCostEl.parent().parent().find("span.arrow");
    if(this._todayCostUp === true) {
        arrow.removeClass("arrow-down").addClass("arrow-up");
    } else if(this._todayCostUp === false) {
        arrow.removeClass("arrow-up").addClass("arrow-down");
    }
}

/**
 * Today kWh setter
 * @param kwh (integer)
 * @param up (true || false || null)
 */
Energomonitor_frontend.prototype.setTodayKwh = function(kwh, up) {
    this._todayKwh = kwh || 0;
    this._todayKwhUp = (typeof up == "boolean") ? up : null;
    this._todayKwhEl.text(this._todayKwh);
    this._setTodayKwhArrow(this._todayKwhUp);
}

/**
 * Today kWh arrow setter
 * @param up (true || false || null)
 * @private
 */
Energomonitor_frontend.prototype._setTodayKwhArrow = function(up) {
    this._todayKwhUp = (typeof up == "boolean") ? up : null;
    this._todayCostEl.attr("data-up", this._todayKwhUp);

    var arrow = this._todayKwhEl.parent().parent().find("span.arrow-small");
    if(this._todayKwhUp === true) {
        arrow.removeClass("arrow-down").addClass("arrow-up");
    } else if(this._todayKwhUp === false) {
        arrow.removeClass("arrow-up").addClass("arrow-down");
    }
}

/**
 * Yesterday cost setter
 * @param cost (integer)
 */
Energomonitor_frontend.prototype.setYesterdayCost = function(cost) {
    this._yesterdayCost = cost || 0;
    this._yesterdayCostEl.text(this._yesterdayCost);
}

/**
 * Yesterday cost setter
 * @param kwh (integer)
 */
Energomonitor_frontend.prototype.setYesterdayKwh = function(kwh) {
    this._yesterdayKwh = kwh || 0;
    this._yesterdayKwhEl.text(this._yesterdayKwh);
}

/**
 * Yesterday whole cost setter
 * @param cost (integer)
 */
Energomonitor_frontend.prototype.setWholeYesterdayCost = function(cost) {
    this._wholeYesterdayCost = cost || 0;
    this._wholeYesterdayCostEl.text(this._wholeYesterdayCost);
}

/**
 * Current month name setter
 * @param name (string)
 */
Energomonitor_frontend.prototype.setCurrentMonth = function(name) {
    this._currentMonthName = name || "";
    this._currentMonthNameEls.text(this._currentMonthName);
}

/**
 * Last month name setter
 * @param name (string)
 */
Energomonitor_frontend.prototype.setLastMonth = function(name) {
    this._lastMonthName = name || "";
    this._lastMonthNameEls.text(this._lastMonthName);
}

/**
 * Current month cost setter
 * @param cost (integer)
 * @param up (true || false || null)
 */
Energomonitor_frontend.prototype.setCurrentMonthCost = function (cost, up) {
    this._currentMonthCost = cost || 0;
    this._currentMonthCostUp = (typeof up == "boolean") ? up : null;
    this._currentMonthCostEl.text(this._currentMonthCost);
    this._setCurrentMonthCostArrow(this._currentMonthCostUp);
}

/**
 * Current month cost arrow setter
 * @param up
 * @private
 */
Energomonitor_frontend.prototype._setCurrentMonthCostArrow = function(up) {
    this._currentMonthCostUp = (typeof up == "boolean") ? up : null;
    this._currentMonthCostEl.attr("data-up", this._currentMonthCostUp);

    var arrow = this._currentMonthCostEl.parent().parent().find("span.arrow");
    if(this._currentMonthCostUp === true) {
        arrow.removeClass("arrow-down").addClass("arrow-up");
    } else if(this._currentMonthCostUp === false) {
        arrow.removeClass("arrow-up").addClass("arrow-down");
    }
}

/**
 * Current month kWh setter
 * @param kwh (integer)
 * @param up (true || false || null)
 */
Energomonitor_frontend.prototype.setCurrentMonthKwh = function(kwh, up) {
    this._currentMonthKwh = kwh || 0;
    this._currentMonthKwhUp = (typeof up == "boolean") ? up : null;
    this._currentMonthKwhEl.text(this._currentMonthKwh);
    this._setCurrentMonthKwhArrow(this._currentMonthKwhUp);
}

/**
 * Today kWh arrow setter
 * @param up (true || false || null)
 * @private
 */
Energomonitor_frontend.prototype._setCurrentMonthKwhArrow = function(up) {
    this._currentMonthKwhUp = (typeof up == "boolean") ? up : null;
    this._currentMonthKwhEl.attr("data-up", this._currentMonthKwhUp);

    var arrow = this._currentMonthKwhEl.parent().parent().find("span.arrow-small");
    if(this._currentMonthKwhUp === true) {
        arrow.removeClass("arrow-down").addClass("arrow-up");
    } else if(this._currentMonthKwhUp === false) {
        arrow.removeClass("arrow-up").addClass("arrow-down");
    }
}

/**
 * Last month cost setter
 * @param cost (integer)
 */
Energomonitor_frontend.prototype.setLastMonthCost = function(cost) {
    this._lastMonthCost = cost || 0;
    this._lastMonthCostEl.text(this._lastMonthCost);
}

/**
 * Last month kWh setter
 * @param kwh (integer)
 */
Energomonitor_frontend.prototype.setLastMonthKwh = function(kwh) {
    this._lastMonthKwh = kwh || 0;
    this._lastMonthKwhEl.text(this._lastMonthKwh);
}

/**
 * Last month whole cost setter
 * @param cost (integer)
 */
Energomonitor_frontend.prototype.setWholeLastMonthCost = function(cost) {
    this._wholeLastMonthCost = cost || 0;
    this._wholeLastMonthCostEl.text(this._wholeLastMonthCost);
}



/**
 * Synchronize selects with dropdowns. (If id is null, all selects will be synced.)
 * @param id || null (#example)
 */
Energomonitor_frontend.prototype.checkSelectboxes = function(id) {
    if(id == null) id = ".select";
    $(id).each(function(index, val) {
        var options = $(val).find("option");
        var button = $(val).find("button");
        var dropdownMenu = $(val).find("ul.dropdown-menu");
        var lis = "";
        options.each(function(key, option) {
            if(option.hasAttribute("selected")) button.find("span").not(".caret").text($(option).text());
            if(options.size() == (key+1)) {
                lis += "<li><a href=\"#\" data-value=\"" + $(option).val() + "\" class=\"last\">" + $(option).text() + "</a></li>";
            } else {
                lis += "<li><a href=\"#\" data-value=\"" + $(option).val() + "\">" + $(option).text() + "</a></li>";
            }
        });
        dropdownMenu.html(lis);
    });
}

/**
 * Event for click on dropdown menu entry (Syncing options with dropdown lis)
 * @param event
 * @private
 */
Energomonitor_frontend.prototype._clickOnDropdown = function(event) {
    event.preventDefault();
    var data = $(event.delegateTarget);
    var _this = $(this);

    // TODO: Better solution
    var button = _this.parent().parent().parent().find("button > span").not(".caret");
    button.text(data.text());

    var select = _this.parent().parent().parent().parent().find("select");
    $("option", select).each(function (key, val) { val.removeAttribute("selected"); });
    $("option[value=" + data.data("value") +"]", select).attr("selected","selected") ;
}


$(document).ready(function() {

    /*
     * Frontend using example

    var em_front = new Energomonitor_frontend();

    //Today
    em_front.setTodayCost(550, false);
    em_front.setTodayKwh(12, true);

    //Yesterday
    em_front.setYesterdayCost(950);
    em_front.setYesterdayKwh(15);
    em_front.setWholeYesterdayCost(1.234);

    //Current month
    em_front.setCurrentMonth("August");
    em_front.setCurrentMonthCost(634, false);
    em_front.setCurrentMonthKwh(13, true);

    //Last month
    em_front.setLastMonth("July");
    em_front.setLastMonthCost(999);
    em_front.setLastMonthKwh(120);
    em_front.setWholeLastMonthCost(1.432);*/
});